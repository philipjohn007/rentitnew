var app = angular.module('project', [ 'ngRoute', 'ngCookies' ]);
app.config(function($locationProvider, $httpProvider, $routeProvider) {
	$routeProvider.when('/plants', {
		controller : 'PlantsController',
		templateUrl : 'views/plants/list.html'
	}).when('/pos', {
		controller : 'POController',
		templateUrl : 'views/pos/list.html'
	}).when('/invoices', {
		controller : 'InvoiceController',
		templateUrl : 'views/invoices/list.html',
	}).when('/login', {
		controller : 'LoginController',
		templateUrl : 'views/login.html'
	}).when('/home', {
		controller : 'HomeController',
		templateUrl : 'views/pos/home.html'
	}).when('/status', {
		controller : 'StatusController',
		templateUrl : 'views/pos/checkstatus.html'
	}).when('/available', {
		controller : 'PlantAvailController',
		templateUrl : 'views/plants/availableplants.html'
	}).when('/availabilityprice', {
		controller : 'AvailabilityController',
		templateUrl : 'views/plants/availabilityprice.html'
	}).otherwise({
		redirectTo : '/login'
	});

	$httpProvider.interceptors
			.push(function($q, $rootScope, $location, $window) {
				return {
					'request' : function(config) {

						var isRestCall = config.url.indexOf('/rest') == 0;
						if (isRestCall) {
							config.headers['Authorization'] = 'Basic '
									+ $rootScope.code;
						}
						return config || $q.when(config);
					},
					'responseError' : function(rejection) {
						var status = rejection.status;
						var config = rejection.config;
						var method = config.method;
						var url = config.url;

						if (status == 401) {
							// $rootScope.logout();
							$window.history.back();
						} else {
							$rootScope.error = method + " on " + url
									+ " failed with status " + status;
						}

						return $q.reject(rejection);
					}
				};
			});

});

app.run(function($rootScope, $location, $cookieStore) {
	var user = $cookieStore.get("user");
	var code = $cookieStore.get("code");
	if (user !== undefined && code !== undefined) {
		$rootScope.user = user;
		$rootScope.code = code;
	}

	$rootScope.logout = function() {
		delete $rootScope.user;
		delete $rootScope.code;

		$cookieStore.remove("user");
		$cookieStore.remove("code");
		$cookieStore.remove("userRoles");
		$location.path("/login");
	};
});

app.service("RoleBasedAccessService", function($rootScope, $location, $q,
		$cookieStore) {
	return {
		permissionCheck : function(roleCollection) {
			var deferred = $q.defer();
			var userRoles = $cookieStore.get("userRoles");

			var matchingRoles = userRoles.filter(function(role) {
				return roleCollection.indexOf(role) != -1;
			});
			if (userRoles !== undefined && matchingRoles.length > 0)
				deferred.resolve();
			else {
				$location.path("/login");
				$rootScope.$on("$locationChangeSuccess",
						function(next, current) {
							deferred.resolve();
						});
			}
			return deferred.promise;
		}
	};
});
