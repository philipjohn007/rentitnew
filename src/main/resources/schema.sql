create table if not exists users (username varchar(50) not null primary key, password varchar(50) not null, enabled boolean not null)
create table if not exists authorities (username varchar(50) not null, authority varchar(50) not null, constraint FK_AUTHORITIES_USERS foreign key(username) references users(username))


insert into users (username, password, enabled) SELECT 'admin', 'admin', true where not exists (select username, password, enabled from users where username = 'admin' and password = 'admin')
insert into authorities (username, authority) SELECT 'admin', 'ROLE_ADMIN' where not exists (select username, authority from authorities where username = 'admin' and authority = 'ROLE_ADMIN')


