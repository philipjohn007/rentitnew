package rentitnew.security;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("/rest/authentication")
@RestController
public class AuthenticationController {
	ObjectMapper mapper = new ObjectMapper();

	@RequestMapping(method = RequestMethod.POST)
	@Secured({ "ROLE_USER", "ROLE_ADMIN" })
	public String authenticate() throws JsonProcessingException {
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = new LinkedList<String>();

		if (principal instanceof UserDetails) {
			UserDetails details = (UserDetails) principal;
			for (GrantedAuthority authority : details.getAuthorities()) {
				roles.add(authority.getAuthority());
			}
		}

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		map.put("roles", roles);

		return mapper.writeValueAsString(map);
	}

	@ExceptionHandler(value = { SecurityException.class })
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public void handleSecurityException() {
	}
}
