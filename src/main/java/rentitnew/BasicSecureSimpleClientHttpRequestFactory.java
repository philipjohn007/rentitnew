package rentitnew;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import rentitnew.Application.Credentials;

public class BasicSecureSimpleClientHttpRequestFactory extends
		SimpleClientHttpRequestFactory {
	@Autowired
	Credentials credentials;

	public BasicSecureSimpleClientHttpRequestFactory() {
	}

	@Override
	public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod)
			throws IOException {
		ClientHttpRequest result = super.createRequest(uri, httpMethod);
		System.out.println(uri);
		System.out.println(uri.getAuthority());
		System.out.println(credentials.getCredentials());

		for (Map<String, String> map : credentials.getCredentials().values()) {
			String authority = map.get("authority");
			if (authority != null && authority.equals(uri.getAuthority())) {
				result.getHeaders().add("Authorization",
						map.get("authorization"));
				break;
			}
		}

		if (credentials.getCredentials().containsKey(uri.getAuthority())) {
		}
		return result;
	}
}