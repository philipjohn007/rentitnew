/**
 *
 */
package rentitnew.services.buildit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import rentitnew.integration.dto.InvoiceResource;
import rentitnew.integration.dto.NotificationResource;
import rentitnew.integration.rest.InvoiceNotFoundException;

@Service
public class BuildService {
	@Autowired
	RestTemplate restTemplate;

	@Value("${buildit.host}")
	String host;

	@Value("${buildit.port}")
	String port;

	public InvoiceResource createInvoice(final InvoiceResource invoice) {
		try {
			final ResponseEntity<InvoiceResource> result = restTemplate
					.postForEntity("http://" + host + port + "/rest/invoices",
							invoice, InvoiceResource.class);

			return result.getBody();
		} catch (final HttpClientErrorException e) {
			return null;
		}
	}

	public NotificationResource createNotification(
			final NotificationResource notify) {
		try {
			final ResponseEntity<NotificationResource> result = restTemplate
					.postForEntity("http://" + host + port
							+ "/rest/notifications", notify,
							NotificationResource.class);

			return result.getBody();
		} catch (final HttpClientErrorException e) {
			return null;
		}
	}

	public void updateInvoice(InvoiceResource res) throws RestClientException,
			InvoiceNotFoundException {

		restTemplate.put("http://" + host + port + "/rest/invoices", res,
				InvoiceResource.class);

	}
}
