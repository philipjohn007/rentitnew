/**
 *
 */
package rentitnew.models;

/**
 * @author philipjohn007
 *
 */
public enum Status {

	PENDING_CONFIRMATION,

	CLOSED,

	REJECTED,

	OPEN,

	PENDING_UPDATE,

	DISPATCHED,

	DELIVERED,

	CUSTOMER_REJECTED,

	RETURNED;
}
