/**
 *
 */
package rentitnew.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * @author philipjohn007
 *
 */
@Entity
@Data
public class PurchaseOrder {
	@Id
	@GeneratedValue
	Long id;

	@OneToOne
	Customer customer;

	@OneToOne
	Plant plant;

	@Temporal(TemporalType.DATE)
	Date startDate;

	@Temporal(TemporalType.DATE)
	Date endDate;

	Float cost;

	@Enumerated(EnumType.STRING)
	Status status;

	public void calcCost(final Plant p) {
		cost = ((endDate.getTime() - startDate.getTime())
				/ (1000 * 60 * 60 * 24) + 1)
				* p.getPrice();
	}
}
