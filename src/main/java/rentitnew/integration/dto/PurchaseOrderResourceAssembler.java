/**
 *
 */
package rentitnew.integration.dto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentitnew.integration.rest.InvoiceRestController;
import rentitnew.integration.rest.PurchaseOrderRestController;
import rentitnew.models.PurchaseOrder;
import rentitnew.util.ExtendedLink;

/**
 * @author philipjohn007
 *
 */
public class PurchaseOrderResourceAssembler extends
		ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderResource> {

	PlantResourceAssembler plantAssembler;

	public PurchaseOrderResourceAssembler() {
		super(PurchaseOrderRestController.class, PurchaseOrderResource.class);
	}

	@Override
	public PurchaseOrderResource toResource(final PurchaseOrder po) {
		final PurchaseOrderResource poResource = createResourceWithId(
				po.getId(), po);
		poResource.setCost(po.getCost());
		poResource.setEndDate(po.getEndDate());
		final PlantResourceAssembler assember = new PlantResourceAssembler();
		poResource.setPlant(assember.toResource(po.getPlant()));
		poResource.setStartDate(po.getStartDate());
		poResource.setStatus(po.getStatus());
		// try {
		switch (po.getStatus()) {
		case PENDING_CONFIRMATION:
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class).rejectPO(
			// po.getId())).toString(), "Reject", "DELETE"));
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class).acceptPO(
			// po.getId())).toString(), "Accept", "POST"));
			// break;
		case PENDING_UPDATE:
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class)
			// .rejectPOUpdate(po.getId())).toString(),
			// "Reject Update", "DELETE"));
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class)
			// .acceptPOUpdate(po.getId())).toString(),
			// "Accept Update", "POST"));
			break;
		case REJECTED:
			// poResource
			// .add(new ExtendedLink(
			// linkTo(
			// methodOn(
			// PurchaseOrderRestController.class)
			// .updatePO(po.getId(), null))
			// .toString(), "Update", "PUT"));
			break;
		case CLOSED:
			break;
		case OPEN:
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class).closePO(
			// po.getId())).toString(), "Close", "DELETE"));
			// poResource.add(new ExtendedLink(linkTo(
			// methodOn(PurchaseOrderRestController.class)
			// .requestPOUpdate(po.getId())).toString(),
			// "Update", "POST"));
			poResource.add(new ExtendedLink(linkTo(
					methodOn(PurchaseOrderRestController.class).dispatchPO(
							po.getId())).toString(), "Dispatch", "POST"));
			break;

		case DISPATCHED:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(PurchaseOrderRestController.class).deliverPO(
							po.getId())).toString(), "Deliver", "POST"));
			poResource.add(new ExtendedLink(linkTo(
					methodOn(PurchaseOrderRestController.class)
							.customerRejectPO(po.getId())).toString(),
					"Customer Reject", "DELETE"));
			break;

		case DELIVERED:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(PurchaseOrderRestController.class).returnPO(
							po.getId())).toString(), "Return", "POST"));
			break;

		case CUSTOMER_REJECTED:
			break;

		case RETURNED:
			poResource.add(new ExtendedLink(linkTo(
					methodOn(InvoiceRestController.class).create(null))
					.toString(), "Send Invoice", "POST"));
			break;
		default:
			break;
		}
		// } catch (PONotFoundException e) {
		//
		// }

		return poResource;
	}

	public List<PurchaseOrderResource> toResource(final List<PurchaseOrder> pos) {
		final List<PurchaseOrderResource> ress = new ArrayList<>();

		for (final PurchaseOrder po : pos) {
			ress.add(toResource(po));
		}
		return ress;
	}

}
