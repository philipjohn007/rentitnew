/**
 *
 */
package rentitnew.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import rentitnew.models.Customer;
import rentitnew.models.Status;
import rentitnew.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author philipjohn007
 *
 */
@Getter
@Setter
@XmlRootElement(name = "purchase_order")
public class PurchaseOrderResource extends ResourceSupport {

	@JsonIgnore
	Long idRes;

	@JsonIgnore
	Customer customer;

	PlantResource plant;

	@DateTimeFormat(iso = ISO.DATE)
	Date startDate;

	@DateTimeFormat(iso = ISO.DATE)
	Date endDate;

	Status status;

	Float cost;
}
