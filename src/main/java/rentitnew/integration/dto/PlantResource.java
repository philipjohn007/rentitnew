/**
 *
 */
package rentitnew.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import rentitnew.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author philipjohn007
 *
 */
@Getter
@Setter
@XmlRootElement(name = "plant")
public class PlantResource extends ResourceSupport {
	@JsonIgnore
	Long idRes;

	String name;
	String description;
	Float price;
}
