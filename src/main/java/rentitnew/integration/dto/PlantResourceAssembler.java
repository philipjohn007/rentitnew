/**
 *
 */
package rentitnew.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentitnew.integration.rest.PlantRestController;
import rentitnew.models.Plant;

/**
 * @author philipjohn007
 *
 */
public class PlantResourceAssembler extends
		ResourceAssemblerSupport<Plant, PlantResource> {

	public PlantResourceAssembler() {
		super(PlantRestController.class, PlantResource.class);
	}

	@Override
	public PlantResource toResource(final Plant plant) {
		final PlantResource res = createResourceWithId(plant.getId(), plant);
		res.setIdRes(plant.getId());
		res.setName(plant.getName());
		res.setDescription(plant.getDescription());
		res.setPrice(plant.getPrice());
		return res;
	}

	public List<PlantResource> toResource(final List<Plant> plants) {
		final List<PlantResource> ress = new ArrayList<>();

		for (final Plant plant : plants) {
			ress.add(toResource(plant));
		}
		return ress;
	}
}
