package rentitnew.integration.dto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentitnew.integration.rest.InvoiceNotFoundException;
import rentitnew.integration.rest.InvoiceRestController;
import rentitnew.models.Invoice;
import rentitnew.util.ExtendedLink;

public class InvoiceResourceAssembler extends
		ResourceAssemblerSupport<Invoice, InvoiceResource> {

	public InvoiceResourceAssembler() {
		super(InvoiceRestController.class, InvoiceResource.class);
	}

	@Override
	public InvoiceResource toResource(Invoice invoice) {
		InvoiceResource res = createResourceWithId(invoice.getId(), invoice);

		res.setStatus(invoice.getStatus());
		res.setIdRes(invoice.getId());
		res.setPoRef(invoice.getPoRef());
		res.setPrice(invoice.getPrice());

		try {
			switch (invoice.getStatus()) {
			case PENDING:
				res.add(new ExtendedLink(linkTo(
						methodOn(InvoiceRestController.class).notify(
								invoice.getId())).toString(), "Notify", "POST"));
				break;
			case REJECTED:
				res.add(new ExtendedLink(linkTo(
						methodOn(InvoiceRestController.class).update(
								invoice.getId())).toString(), "Update", "PUT"));
				break;
			case APPROVED:
				break;
			default:
				break;
			}
		} catch (final InvoiceNotFoundException e) {
			System.out.println("Invoice Not Found!" + e);
		}

		return res;
	}

	public List<InvoiceResource> toResource(final List<Invoice> invoices) {
		List<InvoiceResource> ress = new ArrayList<InvoiceResource>();

		for (final Invoice invoice : invoices) {
			ress.add(toResource(invoice));
		}
		return ress;
	}
}
