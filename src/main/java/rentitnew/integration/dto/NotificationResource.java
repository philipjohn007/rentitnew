/**
 *
 */
package rentitnew.integration.dto;

import lombok.Getter;
import lombok.Setter;
import rentitnew.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class NotificationResource extends ResourceSupport {

	String message;
	String poRef;
	Float price;
}
