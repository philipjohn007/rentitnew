/**
 *
 */
package rentitnew.integration.rest;

/**
 * @author philipjohn007
 *
 */
public class PlantNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlantNotFoundException(final Long id) {
		super(String.format("Plant not found! (Plant id: %d)", id));
	}
}
