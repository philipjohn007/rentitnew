/**
 *
 */
package rentitnew.integration.rest;

/**
 * @author philipjohn007
 *
 */
public class PONotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public PONotFoundException(final Long id) {
		super(String.format("PurchaseOrder not found! (PurchaseOrder id: %d)",
				id));
	}
}
