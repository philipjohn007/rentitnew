package rentitnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rentitnew.integration.dto.InvoiceResource;
import rentitnew.integration.dto.InvoiceResourceAssembler;
import rentitnew.integration.dto.NotificationResource;
import rentitnew.integration.dto.PurchaseOrderResource;
import rentitnew.models.Invoice;
import rentitnew.models.InvoiceStatus;
import rentitnew.models.PurchaseOrder;
import rentitnew.models.Status;
import rentitnew.repositories.InvoiceRepository;
import rentitnew.repositories.PurchaseOrderRepository;
import rentitnew.services.buildit.BuildService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/invoices")
public class InvoiceRestController {

	InvoiceResourceAssembler assembler = new InvoiceResourceAssembler();

	@Autowired
	InvoiceRepository repo;

	@Autowired
	PurchaseOrderRepository poRepo;

	@Autowired
	private BuildService builditProxy;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<InvoiceResource> getAllInvoices() {
		final List<InvoiceResource> resources = assembler.toResource(repo
				.findAll());
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public InvoiceResource getInvoice(@PathVariable final Long id)
			throws InvoiceNotFoundException {
		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			throw new InvoiceNotFoundException(id);
		}
		final InvoiceResource resource = assembler.toResource(invoice);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resource));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resource;
	}

	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public InvoiceResource create(@RequestBody PurchaseOrderResource res) {

		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Invoice invoice = new Invoice();
		invoice.setPoRef(res.getLink("self").getHref());
		invoice.setStatus(InvoiceStatus.PENDING);
		invoice.setPrice(res.getCost());
		Invoice saved = repo.saveAndFlush(invoice);
		InvoiceResource savedRes = assembler.toResource(saved);
		InvoiceResource savedinBuildit = builditProxy.createInvoice(savedRes);
		if (savedinBuildit == null) {
			repo.delete(saved.getId());
			return null;
		}
		PurchaseOrderRestController controller = new PurchaseOrderRestController();
		Long poId = controller.getIdFromLink(savedRes.getPoRef());
		try {
			PurchaseOrder po = poRepo.findOne(poId);
			if (po == null) {
				throw new PONotFoundException(poId);
			}
			po.setStatus(Status.CLOSED);
			po = poRepo.save(po);
		} catch (PONotFoundException e) {

		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public NotificationResource notify(@PathVariable Long id) {
		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			return null;
		}
		NotificationResource notifRes = new NotificationResource();
		notifRes.setPoRef(invoice.getPoRef());
		notifRes.setPrice(invoice.getPrice());
		notifRes.setMessage("Invoice Payment Pending");
		NotificationResource savedRes = builditProxy
				.createNotification(notifRes);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public InvoiceResource update(@PathVariable Long id)
			throws InvoiceNotFoundException {
		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			throw new InvoiceNotFoundException(id);
		}

		PurchaseOrderRestController controller = new PurchaseOrderRestController();
		Long poId = controller.getIdFromLink(invoice.getPoRef());
		try {
			PurchaseOrder po = poRepo.findOne(poId);
			if (po == null) {
				throw new PONotFoundException(poId);
			}
			invoice.setPrice(po.getCost());
		} catch (PONotFoundException e) {

		}

		invoice.setStatus(InvoiceStatus.PENDING);
		Invoice saved = repo.save(invoice);
		InvoiceResource res = assembler.toResource(saved);
		builditProxy.updateInvoice(res);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.PUT)
	public InvoiceResource updateStatus(@RequestBody InvoiceResource res)
			throws InvoiceNotFoundException {
		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Invoice> invoices = repo.findAll();
		Invoice invoice = findInvoiceUsingRefs(invoices, res.getPoRef());
		if (invoice == null) {
			return null;
		}
		invoice.setStatus(res.getStatus());
		Invoice saved = repo.save(invoice);
		InvoiceResource savedRes = assembler.toResource(saved);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	public Invoice findInvoiceUsingRefs(List<Invoice> invoices, String poRef) {
		if (invoices == null) {
			return null;
		}
		for (Invoice invoice : invoices) {
			if (invoice.getPoRef().equals(poRef)) {
				return invoice;
			}
		}
		return null;
	}

	@ExceptionHandler(InvoiceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleInvoiceNotFoundException(final InvoiceNotFoundException ex) {
	}
}
