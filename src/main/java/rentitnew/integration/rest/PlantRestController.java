/**
 *
 */
package rentitnew.integration.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rentitnew.integration.dto.PlantResource;
import rentitnew.integration.dto.PlantResourceAssembler;
import rentitnew.models.Plant;
import rentitnew.repositories.PlantRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author philipjohn007
 *
 */
@RestController
@RequestMapping(value = "/rest/plants")
public class PlantRestController {

	@Autowired
	PlantRepository plantRepo;

	@RequestMapping
	@ResponseStatus(HttpStatus.OK)
	public List<PlantResource> getAll() {
		final List<Plant> plants = plantRepo.findAll();
		final PlantResourceAssembler assembler = new PlantResourceAssembler();
		final List<PlantResource> resources = assembler.toResource(plants);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public PlantResource create(@RequestBody final PlantResource res)
			throws PlantNotFoundException {
		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (res == null) {
			return null;
		}
		final Plant plant = new Plant();
		plant.setName(res.getName());
		plant.setDescription(res.getDescription());
		plant.setPrice(res.getPrice());

		Plant saved = plantRepo.saveAndFlush(plant);
		final PlantResourceAssembler assembler = new PlantResourceAssembler();
		PlantResource savedRes = assembler.toResource(saved);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(value = "{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantResource getPlant(@PathVariable("id") final Long id)
			throws Exception {
		final Plant plant = plantRepo.findOne(id);
		if (plant == null) {
			throw new PlantNotFoundException(id);
		}
		final PlantResourceAssembler assembler = new PlantResourceAssembler();
		final PlantResource res = assembler.toResource(plant);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	@RequestMapping(value = "/availability", method = RequestMethod.GET)
	public List<PlantResource> findAllAvailablePlants(
			@RequestParam(value = "name") String name,
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate) {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = formatter.parse(startDate);
			Date date2 = formatter.parse(endDate);
			final List<Plant> plants = plantRepo.findAllAvailablePlants(name,
					date1, date2);
			if (plants != null) {
				System.out.println("Plant size is " + plants.size());
			}
			final PlantResourceAssembler assembler = new PlantResourceAssembler();
			final List<PlantResource> resources = assembler.toResource(plants);
			ObjectMapper mapper = new ObjectMapper();
			try {
				System.out.println(mapper.writeValueAsString(resources));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return resources;
		} catch (ParseException e) {
			return null;
		}
	}

	@RequestMapping(value = "/plantavailability", method = RequestMethod.GET)
	public boolean checkAvailability(
			@RequestParam(value = "endDate") String endDate,
			@RequestParam(value = "id") String id,
			@RequestParam(value = "startDate") String startDate) {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = formatter.parse(startDate);
			Date date2 = formatter.parse(endDate);
			Long longId = new Long(id);
			List<Plant> plants = plantRepo.isPlantAvailable(longId, date1,
					date2);
			if (plants != null) {
				if (plants.size() > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (ParseException e) {
			return false;
		}
	}

	@RequestMapping(value = "{id}/price", method = RequestMethod.GET)
	public Float checkPrice(@PathVariable("id") final Long id) {
		final Plant plant = plantRepo.findOne(id);
		if (plant == null) {
			return new Float(0);
		}
		Float price = plant.getPrice();
		return price;
	}

	@ExceptionHandler(PlantNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPlantNotFoundException(final PlantNotFoundException ex) {
	}
}
