/**
 *
 */
package rentitnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rentitnew.integration.dto.PlantResource;
import rentitnew.integration.dto.PurchaseOrderResource;
import rentitnew.integration.dto.PurchaseOrderResourceAssembler;
import rentitnew.models.Plant;
import rentitnew.models.PurchaseOrder;
import rentitnew.models.Status;
import rentitnew.repositories.PlantRepository;
import rentitnew.repositories.PurchaseOrderRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author philipjohn007
 *
 */
@RestController
@RequestMapping(value = "/rest/pos")
public class PurchaseOrderRestController {

	@Autowired
	PurchaseOrderRepository poRepo;

	@Autowired
	PlantRepository plantRepo;

	final PurchaseOrderResourceAssembler poAssembler = new PurchaseOrderResourceAssembler();

	@RequestMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource getPO(@PathVariable("id") final Long id)
			throws Exception {
		final PurchaseOrder po = poRepo.findOne(id);
		if (po == null) {
			throw new PONotFoundException(id);
		}
		final PurchaseOrderResource res = poAssembler.toResource(po);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(res));
		return res;
	}

	@RequestMapping
	public List<PurchaseOrderResource> getAll() {
		final List<PurchaseOrder> pos = poRepo.findAll();
		final List<PurchaseOrderResource> resources = poAssembler
				.toResource(pos);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@RequestMapping(value = "/{id}/extend", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> extendPO(
			@PathVariable("id") final Long id,
			@RequestBody final PurchaseOrderResource res)
			throws PONotFoundException {

		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResponseEntity<PurchaseOrderResource> response;

		if (res == null) {
			response = new ResponseEntity<PurchaseOrderResource>(
					HttpStatus.NOT_FOUND);
			return response;

		}

		final PurchaseOrder po = poRepo.findOne(id);
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		po.calcCost(po.getPlant());
		po.setStatus(Status.PENDING_CONFIRMATION);
		PurchaseOrder saved = poRepo.save(po);
		PurchaseOrderResource savedRes = poAssembler.toResource(saved);
		if (plantRepo.isPlantAvailable(po.getPlant().getId(),
				po.getStartDate(), po.getEndDate()).isEmpty()) {
			response = rejectPO(saved.getId());
			savedRes = response.getBody();
			response = new ResponseEntity<PurchaseOrderResource>(savedRes,
					HttpStatus.CONFLICT);
		} else {
			response = acceptPO(saved.getId());
			savedRes = response.getBody();
			response = new ResponseEntity<PurchaseOrderResource>(savedRes,
					HttpStatus.OK);
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(response));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> createPO(
			@RequestBody final PurchaseOrderResource res)
			throws PlantNotFoundException {

		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResponseEntity<PurchaseOrderResource> resp;
		if (res == null) {
			throw new PlantNotFoundException(null);
		}
		final PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		if (res.getPlant() == null || res.getPlant().getLink("self") == null) {
			throw new PlantNotFoundException(null);
		}
		Long plantId = getIdFromLink(res.getPlant().getLink("self").getHref());
		Plant plant = plantRepo.findOne(plantId);
		if (plant != null) {
			po.setPlant(plant);
		}
		po.calcCost(po.getPlant());
		po.setStatus(Status.PENDING_CONFIRMATION);
		PurchaseOrder saved = poRepo.saveAndFlush(po);
		PurchaseOrderResource savedRes = poAssembler.toResource(saved);
		if (plantRepo.isPlantAvailable(po.getPlant().getId(),
				po.getStartDate(), po.getEndDate()).isEmpty()) {
			resp = rejectPO(saved.getId());
			savedRes = resp.getBody();
			resp = new ResponseEntity<PurchaseOrderResource>(savedRes,
					HttpStatus.CONFLICT);
		} else {
			resp = acceptPO(saved.getId());
			savedRes = resp.getBody();
			resp = new ResponseEntity<PurchaseOrderResource>(savedRes,
					HttpStatus.CREATED);
		}

		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}

	@RequestMapping(value = "{id}/acceptPO", method = RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrderResource> rejectPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.REJECTED);
		PurchaseOrder saved = poRepo.save(po);
		PurchaseOrderResource savedRes = poAssembler.toResource(saved);
		final ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				savedRes, HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "{id}/acceptPO", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> acceptPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.OPEN);
		PurchaseOrder saved = poRepo.save(po);
		PurchaseOrderResource savedRes = poAssembler.toResource(saved);
		final ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				savedRes, HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<PurchaseOrderResource> updatePO(
			@PathVariable final Long id,
			@RequestBody final PurchaseOrderResource res)
			throws PONotFoundException {
		ObjectMapper mapper1 = new ObjectMapper();
		try {
			System.out.println(mapper1.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResponseEntity<PurchaseOrderResource> response;

		if (res == null) {
			response = new ResponseEntity<PurchaseOrderResource>(
					HttpStatus.NOT_FOUND);
			return response;
		}

		if (poRepo.findOne(res.getIdRes()) == null) {
			throw new PONotFoundException(res.getIdRes());
		}

		final PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		if (res.getPlant() != null) {
			final PlantResource resource = res.getPlant();
			final Plant plant = new Plant();
			plant.setId(resource.getIdRes());
			plant.setName(resource.getName());
			plant.setDescription(resource.getDescription());
			plant.setPrice(resource.getPrice());
			po.setPlant(plant);
		}

		po.setCustomer(res.getCustomer());
		po.calcCost(po.getPlant());
		po.setId(res.getIdRes());
		po.setStatus(Status.PENDING_CONFIRMATION);
		poRepo.save(po);
		response = new ResponseEntity<PurchaseOrderResource>(res, HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(response));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/rejectPOUpdate", method = RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrderResource> rejectPOUpdate(
			@RequestBody final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.OPEN);
		poRepo.save(po);
		final ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/acceptPOUpdate", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> acceptPOUpdate(
			@RequestBody final Long id) {

		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.OPEN);
		poRepo.save(po);
		final ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrderResource> closePO(
			@PathVariable("id") final Long id) throws PONotFoundException {
		PurchaseOrder po = poRepo.findOne(id);
		if (po == null) {
			throw new PONotFoundException(id);
		}
		po.setStatus(Status.CLOSED);
		po = poRepo.save(po);
		final ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "{id}/updates", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> requestPOUpdate(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.calcCost(po.getPlant());
		po.setStatus(Status.PENDING_CONFIRMATION);
		poRepo.save(po);
		ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{id}/deliver", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> deliverPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.DELIVERED);
		poRepo.save(po);
		ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "/{id}/dispatch", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> dispatchPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.DISPATCHED);
		poRepo.save(po);
		ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "/{id}/deliver", method = RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrderResource> customerRejectPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.CUSTOMER_REJECTED);
		poRepo.save(po);
		ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "/{id}/returned", method = RequestMethod.POST)
	public ResponseEntity<PurchaseOrderResource> returnPO(
			@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		po.setStatus(Status.RETURNED);
		poRepo.save(po);
		ResponseEntity<PurchaseOrderResource> resp = new ResponseEntity<PurchaseOrderResource>(
				poAssembler.toResource(po), HttpStatus.OK);
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resp));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resp;
	}

	@RequestMapping(value = "{id}/status", method = RequestMethod.GET)
	public String checkStatus(@PathVariable("id") final Long id) {
		final PurchaseOrder po = poRepo.findOne(id);
		String status = "";
		if (po != null) {
			status = po.getStatus().name();
		} else {
			status = "NOT FOUND";
		}
		return status;
	}

	@ExceptionHandler(PONotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(final PONotFoundException ex) {
	}

	public Long getIdFromLink(String link) {
		if (link == null || link == "") {
			return null;
		}
		String plantId = link.substring(link.lastIndexOf("/") + 1,
				link.length());
		return Long.parseLong(plantId);
	}
}
