/**
 *
 */
package rentitnew.integration.rest;

public class InvoiceNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvoiceNotFoundException(final Long id) {
		super(String.format("Invoice not found! (Invoice id: %d)", id));
	}
}
