/**
 *
 */
package rentitnew.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rentitnew.models.Plant;

/**
 * @author philipjohn007
 *
 */
@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

	List<Plant> findByNameLike(final String name);

	@Query("select p from Plant p "
			+ "where LOWER(p.name) like LOWER(:name) and p.price between :minimum and :maximum")
	List<Plant> finderMethod(@Param("name") final String name,
			@Param("minimum") final Float minimum,
			@Param("maximum") final Float maximum);

	@Query("select p from Plant p "
			+ "where LOWER(p.name) like LOWER(:name) and p NOT in (select po.plant from PurchaseOrder po "
			+ "where (po.startDate >= :date1 and po.startDate <= :date2  or po.endDate >= :date1 and po.endDate <= :date2) "
			+ "and po.status in ('OPEN','DISPATCHED','DELIVERED','CUSTOMER_REJECTED'))")
	List<Plant> findAllAvailablePlants(@Param("name") final String name,
			@Param("date1") final Date date1, @Param("date2") final Date date2);

	@Query("select p from Plant p where p.id = :id and p NOT in (select po.plant from PurchaseOrder po "
			+ "where (po.startDate >= :date1 and po.startDate <= :date2  or po.endDate >= :date1 and po.endDate <= :date2) "
			+ "and po.status in ('OPEN','DISPATCHED','DELIVERED','CUSTOMER_REJECTED'))")
	List<Plant> isPlantAvailable(@Param("id") final Long id,
			@Param("date1") final Date date1, @Param("date2") final Date date2);
}
