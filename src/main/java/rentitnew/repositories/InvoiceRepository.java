/**
 *
 */
package rentitnew.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rentitnew.models.Invoice;

/**
 * @author philipjohn007
 *
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

}
