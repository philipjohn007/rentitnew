var app = angular.module('project');


app.controller('StatusController', function($scope, $http, $location) {
    $scope.status = '';
    $scope.id = null;
    $scope.statusShown = false;

    $scope.execQuery = function () {
    	var url = '/rest/pos/'+$scope.id+'/status';
        $http.get(url).success(function(data, status, headers, config) {
            $scope.status = data;
            $scope.statusShown = true;
        });
    };
});

app.controller('InvoiceController', function($scope, $http, $route, $location, $rootScope) {
	$scope.invoices = [];
	$scope.po=null;
	$scope.poShown=false;
	$http.get('/rest/invoices').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.invoices = data;
	});	
	
	$scope.follow = function(link) {
		$scope.poShown=false;
	    console.log(link);
	    console.log(link.method);
	    if (link.method == 'POST') {
	    	
	        $http.post(link.href).success(function(data, status, headers, config) {
	            console.log(data);
	            $route.reload();
	            alert('Notification Sent Successfully');
	        });
	    } else if (link.method == 'PUT') {
	    	console.log(link.href);
	    	$http.put(link.href).success(function(data, status, headers, config) {
		        console.log(data);
		        $route.reload();
		    });
	    }
	};
	
	$scope.findPO = function(link) {
		var poId = link.substring(link.lastIndexOf("/")+1, link.length);
		$http.get('/rest/pos/'+poId).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.po = data;
			$scope.poShown=true;
		});	
	};
});

app.controller('PlantAvailController', function($scope, $http, $location) {
	$scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.plantsShown = false;

    $scope.execQuery = function () {
    	var url = '/rest/plants/availability';
        $http.get(url, {params: {name: $scope.name, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
            $scope.plants = data;
            $scope.plantsShown = true;
        });
    };
});

app.controller('AvailabilityController', function($scope, $http, $location) {
    $scope.price = null;
    $scope.availability = '';
    $scope.id = null;
    $scope.availShown = false;
    $scope.priceShown = false;

    $scope.checkAvailability = function () {
    	var url = '/rest/plants/plantavailability';
        $http.get(url, {params: {id: $scope.id, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
            if(data == 'true') {
            	$scope.availability = 'Available';
            } else {
            	$scope.availability = 'Not Available';
            }
            $scope.priceShown = false;
            $scope.availShown = true;
        });
    };
    
    $scope.checkPrice = function () {
    	var url = '/rest/plants/'+$scope.id+'/price';
        $http.get(url).success(function(data, status, headers, config) {
            $scope.price = data;
            $scope.availShown = false;
            $scope.priceShown = true;
        });
    };
});

app.controller('PlantsController', function($scope, $http, $route,$location) {
    $scope.plants = [];
    $http.get('/rest/plants').success(function(data, status, headers, config) {
    	console.log(data);
        $scope.plants = data;
    });
}); 

app.controller('POController', function($scope, $http, $route,$location) {
    $scope.pos = [];
    $http.get('/rest/pos').success(function(data, status, headers, config) {
    	console.log(data);
        $scope.pos = data;
    });
    
    $scope.follow = function(link,po) {
    	console.log(po);
	    console.log(link);
	    console.log(link.method);
	    if (link.method == 'POST') {
	    	console.log(link.href);
	        $http.post(link.href,po).success(function(data, status, headers, config) {
	            console.log(data);
	            $route.reload();
	        });
	    } else if (link.method == 'DELETE') {
	    	$http.delete(link.href).success(function(data, status, headers, config) {
	            console.log(data);
	            $route.reload();
	        }); 
	    }
    };
});

app.controller("LoginController", function($scope, $http, $location, $rootScope, $cookieStore) {
	$scope.username = "";
    $scope.password = "";
    delete $rootScope.user;
    delete $rootScope.code;
    $cookieStore.remove("user");
    $cookieStore.remove("code");                
    $cookieStore.remove("userRoles");

    $scope.login = function () {
        var code = window.btoa($scope.username + ":" + $scope.password);      
        console.log(code);
        $rootScope.user = $scope.username;
        $rootScope.code = code;
        $http.post("/rest/authentication")
            .success(function(data, status, headers, config) {            
            	console.log(data.roles);
    
            	$cookieStore.put("userRoles", data.roles);
                $cookieStore.put("user", $scope.username);
                $cookieStore.put("code", code);             
                $location.path( "/home" );
            }).
            error(function(data, status, headers, config) {
            	                       
                $location.path( "/login" );              
                return false;
              });
    };
});

app.controller("HomeController", function($rootScope, $location, $cookieStore,$location) {
	
});